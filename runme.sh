#!/usr/bin/env bash

# TODO: write plpython function

docker-compose build

docker-compose up -d

until $(curl --output /dev/null --silent --head --fail http://localhost:8000/export_csv/); do
	echo "Waiting for http://localhost:8000/export_csv/ to respond"
    sleep 1
done

time curl http://localhost:8000/export_csv/ -o output.csv

wc -l output.csv

docker-compose down
