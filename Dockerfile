FROM python:3.5-slim

WORKDIR /app
COPY requirements.txt .

ENV DOCKERIZE_VERSION v0.6.1

RUN apt update && \
    apt install --no-install-recommends -y \
    libpq-dev libpq5 wget \
    build-essential && \
    wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
        && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
        && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz && \
    pip install --no-cache-dir -r requirements.txt && \
    apt remove -y build-essential libpq-dev wget && \
    apt autoremove -y && \
    apt clean && \
    rm -rf /var/lib/apt/lists

COPY . /app

ENV PYTHONUNBUFFERED=1
ENV PYTHONIOENCODING=utf-8

ENTRYPOINT ["/bin/bash", "/app/docker-entrypoint.sh"]

CMD ["/usr/local/bin/gunicorn", "--bind", "0.0.0.0:8000", "csv_massive_export.wsgi"]
