
from rest_framework import serializers

from .models import Customer


class CustomerContactsSerializer(serializers.ModelSerializer):
    phones = serializers.RelatedField(read_only=True)
    emails = serializers.RelatedField(read_only=True)

    class Meta:
        model = Customer
        fields = ('id', 'first_name', 'last_name', 'emails', 'phones')
