from django.shortcuts import render
from rest_framework import viewsets, mixins
from rest_framework_csv import renderers
from rest_framework.decorators import action

from .models import Customer
from .serializers import CustomerContactsSerializer


class CSVExportViewSet(viewsets.GenericViewSet,
                       mixins.ListModelMixin):
    queryset = Customer.objects.all()
    renderer_classes = (renderers.CSVStreamingRenderer,)
    serializer_class = CustomerContactsSerializer
    pagination_class = None

